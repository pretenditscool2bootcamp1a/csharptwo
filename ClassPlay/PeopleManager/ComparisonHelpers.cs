﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class DescendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return y.Age - x.Age;
        }
    }
    public class AscendingAgeSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }
    }

    public class DescendingZipSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return String.Compare(x.ZipCode, y.ZipCode, comparisonType: StringComparison.OrdinalIgnoreCase);
        }
    }

    public class AscendingZipSorter : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return -1 * String.Compare(x.ZipCode, y.ZipCode, comparisonType: StringComparison.OrdinalIgnoreCase);
        }
    }

    public class AscendingStateCity : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            int compareInt = String.Compare(x.State, y.State, comparisonType: StringComparison.OrdinalIgnoreCase);
            if (compareInt != 0) { return compareInt; }
            else
            {
                return String.Compare(x.City, y.City, comparisonType: StringComparison.OrdinalIgnoreCase);
            }
        }
    }
}
