﻿using FileProcessing;
using System.Globalization;

namespace ClassPlay
{
    public class Program
    {

        static void Main(string[] args)
        {
            string payrollDirLocation = @"C:\Academy\csharp\datafiles\";
            string[] payrollFileNames = { "PayrollData1.txt", "PayrollData2.txt", "PayrollData3.txt" };
            DateTime runTime = DateTime.Now;
            StreamWriter logfile = null; 
            StreamReader inputPayrollFile = null;
            string logFileName = @"C:\Academy\csharp\datafiles\fileProcessing.log";
            string targetOpen = "";
            int numberOfFieldsPer = 0;
            int lineCounter = 0; 
            try
            {
                using (logfile = new StreamWriter(logFileName, true))
                {
                    foreach (string payrollFileName in payrollFileNames)
                    {
                        logfile.WriteLine("Processing file: {0} on {1} at {2}", payrollDirLocation + payrollFileName[0], runTime.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), runTime.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                        try { 
                            using (inputPayrollFile = new StreamReader(payrollDirLocation + payrollFileName))
                            {
                                lineCounter = 0;
                                while (inputPayrollFile.EndOfStream != true)
                                {
                                    lineCounter++;
                                    string text = inputPayrollFile.ReadLine();
                                    // do the things to the lines
                                    string[] inputFields = text.Split("|");
                                    numberOfFieldsPer = inputFields.Length;
                                   /* if (numberOfFieldsPer != 3)
                                    {
                                        logfile.WriteLine("Payroll File {0}, line {1} , 3 fields required - found {2}", payrollDirLocation + payrollFileNames[0], lineCounter, numberOfFieldsPer);
                                        Console.WriteLine("Payroll File {0}, line {1} , 3 fields required - found {2}", payrollDirLocation + payrollFileNames[0], lineCounter, numberOfFieldsPer);
                                        continue;
                                    }
                                    else // got the required number of fields in the input line . let's try to process them 
                                    {*/
                                        TimeCard timeCard = TimeCard.CreateTimeCard(text);
                                        if (timeCard.Name!= null && timeCard.Name != null && timeCard.PayRate != null && timeCard.PayRate != null)
                                        {
                                        Console.WriteLine("Payroll File: {3} Employee: {0} Total Hours Worked: {1} GrossPay: {2}", timeCard.Name, timeCard.HoursWorked, timeCard.GetGrossPay(), payrollFileName);
                                        }
                                        else
                                    {
                                        Console.WriteLine("Error in file input data: Null values in input line");
                                    }
                                        
                                  /*  }*/
                                } // while
                            } // using inputPayrollFile
                        } // try 
                        catch (FileNotFoundException ex1)
                        {
                            Console.WriteLine("Error: input payroll file not found or can't be opened : " + ex1.Message);
                        }
                        catch (Exception ex2)
                        {
                            Console.WriteLine("Error: input payroll file exception : " + ex2.Message);
                        }
                    } // foreach
                } // using
            } // try 
            catch(FileNotFoundException ex1)
            {
                Console.WriteLine("Error: log file not found or can't be opened : " + ex1.Message);
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Error: log file exception : " + ex2.Message);
            }
            finally
            {
                if (logfile != null)
                {
                    logfile.Close();
                }
                if (inputPayrollFile != null)
                {
                    inputPayrollFile.Close();   
                }
            } // finally 
        } // static void main  
    } // class program 
} // namespace 