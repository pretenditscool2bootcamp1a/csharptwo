﻿
using StudentAdvising;

string command = "";
double returnValueDbl = 0.0;
/*string returnValueString = "";*/
string advisingLocation = "";
string scrubCharString = "";
string replaceCharString = "";
char scrubChar ;
char replaceChar ;

while (command != "QUIT")
{
    returnValueDbl = 0.0;
    command = "";
    advisingLocation = "";
    scrubCharString = "";
    replaceCharString = "";

    Console.WriteLine("Commands -->");
    Console.WriteLine(" Student: Student Advising");
    Console.WriteLine(" Scrub : Scrub leading and Trailing spaces from a phone number");
    Console.WriteLine(" ScrubChar : Scrub a character from a phone number and leading and Trailing spaces");
    Console.WriteLine(" ScrubReplace : Scrub and replace a character from a phone number with another Char");
    Console.WriteLine(" CtoF : Convert a Celsius temp to Farenheit");
    Console.WriteLine(" FtoC : Convert a Farenheit temp to Celsius");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "Student")
    {
        try
        {
            GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification);
            advisingLocation = GetAdvisingLocation(ref inputStudentMajor, inputStudentClassification);
            Console.WriteLine("Advising for {3} who is a {0} {1} majors is located at : {2}", inputStudentMajor, inputStudentClassification, advisingLocation, inputStudentName);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    } 
    else if (command == "CtoF")
    {
        try
        {
            Console.Write("Enter a Celsius temp to convert to Farenheit: ");
            string inputStringCelsius = Console.ReadLine();
            returnValueDbl = convertCtoF(inputStringCelsius);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", returnValueDbl, inputStringCelsius);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "FtoC")
    {
        try
        {
            Console.Write("Enter a Farenheit temp to convert to Celsius: ");
            string inputStringFarenheit = Console.ReadLine();
            returnValueDbl = convertFtoC(inputStringFarenheit);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", inputStringFarenheit, returnValueDbl);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Scrub")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();
            DataScrubber.ScrubPhone(ref PhoneNumber);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubChar")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            DataScrubber.ScrubPhone(ref PhoneNumber, scrubCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubReplace")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            Console.Write("Enter a character to replace the scrubbed character: ");
            replaceCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            if (replaceCharString != null)
            {
                replaceChar = replaceCharString[0];
            }
            else { replaceChar = ' '; }

            DataScrubber.ScrubPhone(ref PhoneNumber, scrubCharString, replaceCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}

static double convertFtoC(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        farenheitTemperature = Convert.ToDouble(args);
        celsiusTemperature = (farenheitTemperature - 32.0) * 5 / 9;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine(" convertFtoC function: Failed to convert string argument of farenheit temp to double");
    }
    return celsiusTemperature;
}

static double convertCtoF(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        celsiusTemperature = Convert.ToDouble(args);
        farenheitTemperature = ((celsiusTemperature * 9.0) / 5) + 32;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine("convertCtoF function: Failed to convert string argument of celsius temp to double");
    }
    return farenheitTemperature;
}



static void GetData (out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification){
    Console.Write("What is the student's name? ");
    inputStudentName = Console.ReadLine();
    Console.Write("What is the student's major? ");
    inputStudentMajor = Console.ReadLine();
    inputStudentMajor.ToUpper();
    Console.Write("What is the student's classification? ");
    inputStudentClassification = Console.ReadLine();
}
static string GetAdvisingLocation(ref string Major, string Classification) {
    string location = "";
    if (Major != null && Classification != null) { 
        if (Major == "BIOL" && (Classification == "Freshman" || Classification == "Sophomore"))
        {
            location = "Science Bldg, Room 310";
        }
        else if (Major == "BIOL" && (Classification == "Junior" || Classification == "Senior"))
        {
            location = "Science Bldg, Room 311";
        }
        else if (Major == "CSCI")
        {
            location = "Sheppard Hall, Room 314";
        }
        else if (Major == "ENG" && (Classification == "Freshman"))
        {
            location = "Kerr Hall, Room 201";
        }
        else if (Major == "ENG" && (Classification == "Junior" || Classification == "Senior" || Classification == "Sophomore"))
        {
            location = "Kerr Hall, Room 312";
        }
        else if (Major == "HIST")
        {
            location = "Kerr Hall, Room 114";
        }
        else if (Major == "MKT" && (Classification == "Freshman" || Classification == "Sophomore" || Classification == "Junior"))
        {
            location = "Westly Hall, Room 310";
        }
        else if (Major == "MKT" && (Classification == "Senior"))
        {
            location = "Westly Hall, Room 313";
        }
    }
    if (Major != null)
    {
        switch (Major)
        {
            case "ENG":
                Major = "English";
                break;
            case "BIOL":
                Major = "Biology";
                break;
            case "CSCI":
                Major = "Computer Science";
                break;
            case "HIST":
                Major = "History";
                break;
            case "MKT":
                Major = "Marketing";
                break;
            default:
                Major = "Unknown Major";
                break;
        }
    }

    return location; 
}