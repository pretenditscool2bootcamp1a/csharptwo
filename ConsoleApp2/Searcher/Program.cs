﻿using System.Diagnostics;
using System.Numerics;
using TerminationDates;

string command = "";
double returnValueDbl = 0.0;
/*string returnValueString = "";*/
string advisingLocation = "";
string scrubCharString = "";
string replaceCharString = "";
char scrubChar;
char replaceChar;

while (command != "QUIT")
{
    returnValueDbl = 0.0;
    command = "";
    advisingLocation = "";
    scrubCharString = "";
    replaceCharString = "";

    Console.WriteLine("Commands -->");
    Console.WriteLine(" Searcher : Search menu array for an item");
    Console.WriteLine(" TerminationDate : Calculate insurance policy renewal date");
    Console.WriteLine(" Greeter : Display a greeting based on the time of day");
    Console.WriteLine(" ParseName : Parse a name into it's name parts");
    Console.WriteLine(" Student: Student Advising");
    Console.WriteLine(" Scrub : Scrub leading and Trailing spaces from a phone number");
    Console.WriteLine(" ScrubChar : Scrub a character from a phone number and leading and Trailing spaces");
    Console.WriteLine(" ScrubReplace : Scrub and replace a character from a phone number with another Char");
    Console.WriteLine(" CtoF : Convert a Celsius temp to Farenheit");
    Console.WriteLine(" FtoC : Convert a Farenheit temp to Celsius");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();

    if (command == "Searcher")
    {
        try
        {
            string[] items = { "Sausage Breakfast Taco", "Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit", "Bacon and Egg Biscuit", "Pancakes" };
            decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };
            Console.Write("Enter and item from the breakfast menu: ");
            string selectedItem = Console.ReadLine();
            decimal? itemPrice = searchMenu(items, prices, selectedItem);
            if (itemPrice is null)
            {
                Console.WriteLine("We couldn't find that item, please try again");
            }
            else
            {
                Console.WriteLine("That item costs {0:c}", itemPrice);
            }
        }
        catch (Exception ex) 
        {
            Console.WriteLine("Error: Not a valid date time string entered");
        }
    }
    else if (command == "TerminationDate")
    {
        try
        {
            Console.Write("Enter the policy renweal date: ");
            string inputStringRenewalDate = Console.ReadLine();

            bool didParseDate = DateTime.TryParse(inputStringRenewalDate, out DateTime RenewalDate);
            if (didParseDate)
            {
                DateTime gracePeriodDate = RenewalDate.AddDays(10);
                DateTime cancellationDate = RenewalDate.AddMonths(1);
                Console.WriteLine("Renewal Date Entered: {0:d} 10 day Grace period falls on: {1:d} lack of payment cancellation date: {2:d}", RenewalDate, gracePeriodDate, cancellationDate);
            }
            else
            {
                Console.WriteLine("Error: Not a valid date time string entered");
            }


        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Greeter")
    {
        DateTime whatTimeItIs = DateTime.Now;
        if (whatTimeItIs.Hour >= 5 && whatTimeItIs.Hour < 10)
        {
            Console.WriteLine("Good Morning, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 10 && whatTimeItIs.Hour < 18)
        {
            Console.WriteLine("Good Day, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 18 && whatTimeItIs.Hour <= 23)
        {
            Console.WriteLine("Good Evening, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 0 && whatTimeItIs.Hour < 5)
        {
            Console.WriteLine("Welcome to the late shift!, the time is {0}", whatTimeItIs);
        }
    }
    else if (command == "ParseName")
    {
        try
        {
            NameParsing.GetName(out string fullName, out string firstName, out string lastName, out string middleName);
            Console.WriteLine("Name: {0} FirstName: {1} MiddleName: {2} LastName: {3}", fullName, firstName, middleName, lastName);

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Student")
    {
        try
        {
            GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification);
            advisingLocation = GetAdvisingLocation(ref inputStudentMajor, inputStudentClassification);
            Console.WriteLine("Advising for {3} who is a {0} {1} majors is located at : {2}", inputStudentMajor, inputStudentClassification, advisingLocation, inputStudentName);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "CtoF")
    {
        try
        {
            Console.Write("Enter a Celsius temp to convert to Farenheit: ");
            string inputStringCelsius = Console.ReadLine();
            returnValueDbl = convertCtoF(inputStringCelsius);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", returnValueDbl, inputStringCelsius);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "FtoC")
    {
        try
        {
            Console.Write("Enter a Farenheit temp to convert to Celsius: ");
            string inputStringFarenheit = Console.ReadLine();
            returnValueDbl = convertFtoC(inputStringFarenheit);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", inputStringFarenheit, returnValueDbl);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Scrub")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();
            NameParsing.ScrubPhone(ref PhoneNumber);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubChar")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            NameParsing.ScrubPhone(ref PhoneNumber, scrubCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubReplace")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            Console.Write("Enter a character to replace the scrubbed character: ");
            replaceCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            if (replaceCharString != null)
            {
                replaceChar = replaceCharString[0];
            }
            else { replaceChar = ' '; }

            NameParsing.ScrubPhone(ref PhoneNumber, scrubCharString, replaceCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}

static double convertFtoC(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        farenheitTemperature = Convert.ToDouble(args);
        celsiusTemperature = (farenheitTemperature - 32.0) * 5 / 9;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine(" convertFtoC function: Failed to convert string argument of farenheit temp to double");
    }
    return celsiusTemperature;
}

static double convertCtoF(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        celsiusTemperature = Convert.ToDouble(args);
        farenheitTemperature = ((celsiusTemperature * 9.0) / 5) + 32;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine("convertCtoF function: Failed to convert string argument of celsius temp to double");
    }
    return farenheitTemperature;
}


static void GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification)
{
    Console.Write("What is the student's name? ");
    inputStudentName = Console.ReadLine();
    Console.Write("What is the student's major? ");
    inputStudentMajor = Console.ReadLine();
    inputStudentMajor.ToUpper();
    Console.Write("What is the student's classification? ");
    inputStudentClassification = Console.ReadLine();
}
static string GetAdvisingLocation(ref string Major, string Classification)
{
    string location = "";
    if (Major != null && Classification != null)
    {
        if (Major == "BIOL" && (Classification == "Freshman" || Classification == "Sophomore"))
        {
            location = "Science Bldg, Room 310";
        }
        else if (Major == "BIOL" && (Classification == "Junior" || Classification == "Senior"))
        {
            location = "Science Bldg, Room 311";
        }
        else if (Major == "CSCI")
        {
            location = "Sheppard Hall, Room 314";
        }
        else if (Major == "ENG" && (Classification == "Freshman"))
        {
            location = "Kerr Hall, Room 201";
        }
        else if (Major == "ENG" && (Classification == "Junior" || Classification == "Senior" || Classification == "Sophomore"))
        {
            location = "Kerr Hall, Room 312";
        }
        else if (Major == "HIST")
        {
            location = "Kerr Hall, Room 114";
        }
        else if (Major == "MKT" && (Classification == "Freshman" || Classification == "Sophomore" || Classification == "Junior"))
        {
            location = "Westly Hall, Room 310";
        }
        else if (Major == "MKT" && (Classification == "Senior"))
        {
            location = "Westly Hall, Room 313";
        }
    }
    if (Major != null)
    {
        switch (Major)
        {
            case "ENG":
                Major = "English";
                break;
            case "BIOL":
                Major = "Biology";
                break;
            case "CSCI":
                Major = "Computer Science";
                break;
            case "HIST":
                Major = "History";
                break;
            case "MKT":
                Major = "Marketing";
                break;
            default:
                Major = "Unknown Major";
                break;
        }
    }

    return location;
}
static decimal? searchMenu(string[] items,decimal[] prices, string selectedItem)
{
    try { 
    int itemPosition = Array.IndexOf(items, selectedItem);
    decimal pricedItem = prices[itemPosition];
        return pricedItem;
    }
    catch (Exception ex) 
    { 
        return null;
    }
}
