﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ToDoTracker
{
    public class ToDoTask : IComparable<ToDoTask>
    {
        public string TaskID { get; set; }
        public string Description { get; set; }
        public int Difficulty { get; set; }
        public string Assigned_To_Name { get; set; }
        public DateTime DueDate { get; set; }

        public ToDoTask(string taskID, string description, int difficulty, string assigned_To_Name, DateTime dueDate)
        {
            TaskID = taskID;
            Description = description;
            Difficulty = difficulty;
            Assigned_To_Name = assigned_To_Name;
            DueDate = dueDate;
        }

        public override string ToString() 
        {
            return ("TaskID: " + this.TaskID + " Description: " + this.Description 
                + " Difficulty: " + this.Difficulty.ToString() + " Assigned to Name: " 
                + this.Assigned_To_Name + " DueDate: " + this.DueDate.ToShortDateString());
        }

        public int CompareTo(ToDoTask? other)
        {
            return TaskID.CompareTo(other.TaskID);
        }
    }
}
