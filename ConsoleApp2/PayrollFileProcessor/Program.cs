﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks.Sources;
using FileTest;
using System.IO;
using System.Globalization;

string command = "";
string subCommand = "";
double returnValueDbl = 0.0;
/*string returnValueString = "";*/
string advisingLocation = "";
string scrubCharString = "";
string replaceCharString = "";
char scrubChar;
char replaceChar;

while (command != "QUIT")
{
    returnValueDbl = 0.0;
    command = "";
    subCommand = "";
    advisingLocation = "";
    scrubCharString = "";
    replaceCharString = "";

    Console.WriteLine("Commands -->");
    Console.WriteLine(" Payroll : Process Payroll Files");
    Console.WriteLine(" Files : access file functions");
    Console.WriteLine(" Multi : Display multiplication tables");
    Console.WriteLine(" Statistics : access various arrage statistics methods");
    Console.WriteLine(" Searcher : Search menu array for an item");
    Console.WriteLine(" TerminationDate : Calculate insurance policy renewal date");
    Console.WriteLine(" Greeter : Display a greeting based on the time of day");
    Console.WriteLine(" ParseName : Parse a name into it's name parts");
    Console.WriteLine(" Student: Student Advising");
    Console.WriteLine(" Scrub : Scrub leading and Trailing spaces from a phone number");
    Console.WriteLine(" ScrubChar : Scrub a character from a phone number and leading and Trailing spaces");
    Console.WriteLine(" ScrubReplace : Scrub and replace a character from a phone number with another Char");
    Console.WriteLine(" CtoF : Convert a Celsius temp to Farenheit");
    Console.WriteLine(" FtoC : Convert a Farenheit temp to Celsius");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();

    if (command == "Payroll")
    {
        DirectoryInfo workingDirInfo = new DirectoryInfo(@"C:\Academy\images");
        Console.WriteLine("These are the current Payroll files:");
        foreach (var fi in workingDirInfo.GetFiles("Payroll*.txt"))
        {
            Console.WriteLine(fi.Name);
        }
        Console.Write("Please Enter one of their file names to process: ");
        DateTime whatTimeItIs = DateTime.Now;
        string inputFileNameString = Console.ReadLine();
        string inputFileName = @"C:\Academy\images\" + inputFileNameString;
        string logFileName = @"C:\Academy\images\PayrollFileProcessor.log";
        decimal grossPayTotal = 0;
        decimal grossPayEmployee = 0;
        decimal employeePayRate = 0;
        decimal employeeHours = 0;
        decimal employeeOvertime = 0;
        try
        {
            using (StreamWriter logfile = new StreamWriter(logFileName, true))
            {
                logfile.WriteLine("Processing file: {0} on {1} at {2}", inputFileName, whatTimeItIs.ToString("d",CultureInfo.CreateSpecificCulture("en-US")),  whatTimeItIs.ToString("t",CultureInfo.CreateSpecificCulture("en-us")));
                using (StreamReader inputFile = new StreamReader(inputFileName))
                {
                    while (inputFile.EndOfStream != true)
                    {
                        grossPayEmployee = 0;
                        employeePayRate = 0;
                        employeeHours = 0;
                        employeeOvertime = 0;
                        string text = inputFile.ReadLine();
                        // do the things to the lines
                        string[] inputFields = text.Split(",");
                        employeePayRate = Convert.ToDecimal(inputFields[2]);
                        if (employeeHours <= 40)
                        {
                            employeeHours = Convert.ToDecimal(inputFields[3]);
                            employeeOvertime = 0;
                        }
                        else
                        {
                            employeeHours = 40;
                            employeeOvertime = Convert.ToDecimal(inputFields[3]) - 40 ;
                        }
                        grossPayEmployee = (employeePayRate * employeeHours) + (employeeOvertime * employeePayRate * 1.5M);
                        grossPayTotal = grossPayTotal + grossPayEmployee;
                        Console.WriteLine("Employee number: {0} Name: {1} pay rate: {2:c} hours: {3} earned: {4:c}", inputFields[0], inputFields[1], employeePayRate, inputFields[3], grossPayEmployee);
                    }
                    Console.WriteLine("Gross Pay Totals were {0:c}", grossPayTotal);
                    logfile.WriteLine("Gross Pay Totals were {0:c}", grossPayTotal);
                    logfile.WriteLine("--------------------------------------------");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }

    }
    else if (command == "Files")
    {
        while (subCommand != "QUIT")
        {

            DirectoryInfo workingDirInfo = new DirectoryInfo(@"C:\Academy\images");
            Console.WriteLine("Enter the Files command you want to run");
            Console.WriteLine(" Discover : discover and display all files in the C:/Academy/temp folder ");
            Console.WriteLine(" Create : Create a file in the C:/Academy/temp ");
            Console.WriteLine(" Copy : Copy a file in the C:/Academy/temp ");
            Console.WriteLine(" QUIT : Quit out of the StatisticsManager");
            Console.Write("Enter your command: ");
            subCommand = Console.ReadLine();
            if (subCommand == "Discover")
            {
                Console.WriteLine("Files in @\"C:\\Academy\\images\"");
                foreach (var fi in workingDirInfo.GetFiles())
                {
                    Console.WriteLine(fi.Name);
                }
            }
            else if (subCommand == "Create")
            {
                Console.Write("Enter Filename to create in target directory:");
                string fileName = Console.ReadLine();
                string currentFile = @"C:\Academy\images\" + fileName;
                // Make sure the new file doesn't exist
                if (File.Exists(currentFile))
                {
                    Console.WriteLine($"Error: {currentFile} already exist!");
                    return;
                }
                else
                {
                    File.Create(currentFile).Close();
                    Console.WriteLine("File {0} created", currentFile);
                }
            }
            else if (subCommand == "Copy")
            {
                Console.Write("Enter Filename to copy in target directory:");
                string fileName = Console.ReadLine();
                string currentFile = @"C:\Academy\images\" + fileName;
                DateTime whatTimeItIs = DateTime.Now;
                string newFile = @"C:\Academy\images\" + fileName.Substring(0, fileName.IndexOf(".")) + "-backup-" + whatTimeItIs.Year + whatTimeItIs.Month + whatTimeItIs.Day + whatTimeItIs.Hour + whatTimeItIs.Minute + whatTimeItIs.Second + fileName.Substring(fileName.IndexOf("."));
                // Make sure the source file does exist
                if (!File.Exists(currentFile))
                {
                    Console.WriteLine($"Error: {currentFile} doesn't exist!");
                    return;
                }
                // Make sure the new file doesn't exist
                if (File.Exists(newFile))
                {
                    Console.WriteLine($"Error: {newFile} already exist!");
                    return;
                }
                // Copy the file. Fail if the new file name already exists
                Console.Write("Are you sure you want to copy {0} to {1} ? Y/N : ", currentFile, newFile);
                string stringProceed = Console.ReadLine();
                if (stringProceed == "Y")
                {
                    File.Copy(currentFile, newFile, true);
                }
                else
                {
                    command = "QUIT";
                    continue;
                }

            }
            else if (command != "QUIT")
            {
                Console.WriteLine("**Error: unrecognized command");
            }
        }
    }
    else if (command == "Multi")
    {
        Console.WriteLine("        0     1     2     3     4");
        Console.WriteLine("      ___   ___   ___   ___   ___");
        Console.WriteLine(" ");
        int[,] mathTable = NameParsing.CreateArray();
        for (int i = 0; i < mathTable.GetLength(0); i++) // 10
        {
            Console.Write("{0} |", i);
            for (int j = 0; j < mathTable.GetLength(1); j++) // 5
            {
                mathTable[i, j] = i * j; // multiplication table
                Console.Write("{0, 6}", mathTable[i, j]);
            }
            Console.WriteLine(" ");
        }

    }
    else if (command == "Statistics")
    {
        string[] scoresStringArray = new string[7];
        double[] scoresDoubleArray = new double[7];
        int stringPosition = 0;
        string tempStringScores = "";
        Console.Write("Enter up to 7 scores as integers separated by commas (0s will be skipped): ");
        try
        {
            string inputStringScores = Console.ReadLine();
            tempStringScores = inputStringScores;
            for (int inputStringElementCounter = 0; inputStringElementCounter <= 7; inputStringElementCounter++) // loop up to 7 times to load the incoming string into string array
            {
                tempStringScores = tempStringScores.Substring(stringPosition);
                if (tempStringScores.IndexOf(",") == -1) // if there are no more comma sep'd elements in the incoming string we need to assign the remainder and break out of the loop
                {
                    scoresStringArray[inputStringElementCounter] = tempStringScores;
                    break;
                }
                else
                {
                    stringPosition = tempStringScores.IndexOf(",");
                    scoresStringArray[inputStringElementCounter] = tempStringScores.Substring(0, stringPosition);
                    stringPosition = stringPosition + 1; // need to account for the comma on the next iteration 
                }
            }
            for (int i = 0; i < scoresStringArray.Length; i++) // we have our string array made from the input string now convert to double and load them into the double array
            {
                if (scoresStringArray[i] != null)
                {
                    scoresDoubleArray[i] = Convert.ToDouble(scoresStringArray[i]);
                }
                else
                {
                    break;
                }
            }
            // now we're actually ready to do something with our double array .
            while (subCommand != "QUIT")
            {
                double statisticsFunctionReturnValue = 0;
                Console.WriteLine("Enter the Statistics command you want to run");
                Console.WriteLine(" Average : Get average of input values");
                Console.WriteLine(" Median : Get median of input values");
                Console.WriteLine(" Largest : Get largest of input values");
                Console.WriteLine(" Smallest : Get smallest of input values");
                Console.WriteLine(" QUIT : Quit out of the StatisticsManager");
                Console.Write("Enter your command: ");
                subCommand = Console.ReadLine();
                if (subCommand == "Average")
                {
                    statisticsFunctionReturnValue = GetAverage(scoresDoubleArray);
                    Console.WriteLine("The Average of {0} is {1}", inputStringScores, statisticsFunctionReturnValue);
                }
                else if (subCommand == "Median")
                {
                    statisticsFunctionReturnValue = GetMedian(scoresDoubleArray);
                    Console.WriteLine("The Mean of {0} is {1}", inputStringScores, statisticsFunctionReturnValue);
                }
                else if (subCommand == "Largest")
                {
                    statisticsFunctionReturnValue = GetLargest(scoresDoubleArray);
                    Console.WriteLine("The Largest number of {0} is {1}", inputStringScores, statisticsFunctionReturnValue);
                }
                else if (subCommand == "Smallest")
                {
                    statisticsFunctionReturnValue = GetSmallest(scoresDoubleArray);
                    Console.WriteLine("The Smallest number of {0} is {1}", inputStringScores, statisticsFunctionReturnValue);
                }
                else if (command != "QUIT")
                {
                    Console.WriteLine("**Error: unrecognized command");
                }
            }
        }
        catch (Exception Ex)
        {
            Console.WriteLine($"Error: {Ex.Message}");
        }
    }
    else if (command == "Searcher")
    {
        string[] items = { "Sausage Breakfast Taco", "Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit", "Bacon and Egg Biscuit", "Pancakes" };
        decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };
        Console.Write("Enter and item from the breakfast menu: ");
        string selectedItem = Console.ReadLine();
        decimal? itemPrice = searchMenu(items, prices, selectedItem);
        if (itemPrice is null)
        {
            Console.WriteLine("We couldn't find that item, please try again");
        }
        else
        {
            Console.WriteLine("That item costs {0:c}", itemPrice);
        }
    }
    else if (command == "TerminationDate")
    {
        try
        {
            Console.Write("Enter the policy renweal date: ");
            string inputStringRenewalDate = Console.ReadLine();

            bool didParseDate = DateTime.TryParse(inputStringRenewalDate, out DateTime RenewalDate);
            if (didParseDate)
            {
                DateTime gracePeriodDate = RenewalDate.AddDays(10);
                DateTime cancellationDate = RenewalDate.AddMonths(1);
                Console.WriteLine("Renewal Date Entered: {0:d} 10 day Grace period falls on: {1:d} lack of payment cancellation date: {2:d}", RenewalDate, gracePeriodDate, cancellationDate);
            }
            else
            {
                Console.WriteLine("Error: Not a valid date time string entered");
            }


        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Greeter")
    {
        DateTime whatTimeItIs = DateTime.Now;
        if (whatTimeItIs.Hour >= 5 && whatTimeItIs.Hour < 10)
        {
            Console.WriteLine("Good Morning, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 10 && whatTimeItIs.Hour < 18)
        {
            Console.WriteLine("Good Day, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 18 && whatTimeItIs.Hour <= 23)
        {
            Console.WriteLine("Good Evening, the time is {0}", whatTimeItIs);
        }
        else if (whatTimeItIs.Hour >= 0 && whatTimeItIs.Hour < 5)
        {
            Console.WriteLine("Welcome to the late shift!, the time is {0}", whatTimeItIs);
        }
    }
    else if (command == "ParseName")
    {
        try
        {
            NameParsing.GetName(out string fullName, out string firstName, out string lastName, out string middleName);
            Console.WriteLine("Name: {0} FirstName: {1} MiddleName: {2} LastName: {3}", fullName, firstName, middleName, lastName);

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Student")
    {
        try
        {
            GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification);
            advisingLocation = GetAdvisingLocation(ref inputStudentMajor, inputStudentClassification);
            Console.WriteLine("Advising for {3} who is a {0} {1} majors is located at : {2}", inputStudentMajor, inputStudentClassification, advisingLocation, inputStudentName);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "CtoF")
    {
        try
        {
            Console.Write("Enter a Celsius temp to convert to Farenheit: ");
            string inputStringCelsius = Console.ReadLine();
            returnValueDbl = convertCtoF(inputStringCelsius);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", returnValueDbl, inputStringCelsius);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "FtoC")
    {
        try
        {
            Console.Write("Enter a Farenheit temp to convert to Celsius: ");
            string inputStringFarenheit = Console.ReadLine();
            returnValueDbl = convertFtoC(inputStringFarenheit);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", inputStringFarenheit, returnValueDbl);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Scrub")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();
            NameParsing.ScrubPhone(ref PhoneNumber);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubChar")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            NameParsing.ScrubPhone(ref PhoneNumber, scrubCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "ScrubReplace")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();

            Console.Write("Enter a character to scrub Scrub from the phone number: ");
            scrubCharString = Console.ReadLine();

            Console.Write("Enter a character to replace the scrubbed character: ");
            replaceCharString = Console.ReadLine();

            if (scrubCharString != null)
            {
                scrubChar = scrubCharString[0];
            }
            else { scrubChar = '-'; }

            if (replaceCharString != null)
            {
                replaceChar = replaceCharString[0];
            }
            else { replaceChar = ' '; }

            NameParsing.ScrubPhone(ref PhoneNumber, scrubCharString, replaceCharString);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}

static double convertFtoC(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        farenheitTemperature = Convert.ToDouble(args);
        celsiusTemperature = (farenheitTemperature - 32.0) * 5 / 9;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine(" convertFtoC function: Failed to convert string argument of farenheit temp to double");
    }
    return celsiusTemperature;
}

static double convertCtoF(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        celsiusTemperature = Convert.ToDouble(args);
        farenheitTemperature = ((celsiusTemperature * 9.0) / 5) + 32;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine("convertCtoF function: Failed to convert string argument of celsius temp to double");
    }
    return farenheitTemperature;
}


static void GetData(out string inputStudentName, out string inputStudentMajor, out string inputStudentClassification)
{
    Console.Write("What is the student's name? ");
    inputStudentName = Console.ReadLine();
    Console.Write("What is the student's major? ");
    inputStudentMajor = Console.ReadLine();
    inputStudentMajor.ToUpper();
    Console.Write("What is the student's classification? ");
    inputStudentClassification = Console.ReadLine();
}
static string GetAdvisingLocation(ref string Major, string Classification)
{
    string location = "";
    if (Major != null && Classification != null)
    {
        if (Major == "BIOL" && (Classification == "Freshman" || Classification == "Sophomore"))
        {
            location = "Science Bldg, Room 310";
        }
        else if (Major == "BIOL" && (Classification == "Junior" || Classification == "Senior"))
        {
            location = "Science Bldg, Room 311";
        }
        else if (Major == "CSCI")
        {
            location = "Sheppard Hall, Room 314";
        }
        else if (Major == "ENG" && (Classification == "Freshman"))
        {
            location = "Kerr Hall, Room 201";
        }
        else if (Major == "ENG" && (Classification == "Junior" || Classification == "Senior" || Classification == "Sophomore"))
        {
            location = "Kerr Hall, Room 312";
        }
        else if (Major == "HIST")
        {
            location = "Kerr Hall, Room 114";
        }
        else if (Major == "MKT" && (Classification == "Freshman" || Classification == "Sophomore" || Classification == "Junior"))
        {
            location = "Westly Hall, Room 310";
        }
        else if (Major == "MKT" && (Classification == "Senior"))
        {
            location = "Westly Hall, Room 313";
        }
    }
    if (Major != null)
    {
        switch (Major)
        {
            case "ENG":
                Major = "English";
                break;
            case "BIOL":
                Major = "Biology";
                break;
            case "CSCI":
                Major = "Computer Science";
                break;
            case "HIST":
                Major = "History";
                break;
            case "MKT":
                Major = "Marketing";
                break;
            default:
                Major = "Unknown Major";
                break;
        }
    }

    return location;
}
static decimal? searchMenu(string[] items, decimal[] prices, string selectedItem)
{
    try
    {
        int itemPosition = Array.IndexOf(items, selectedItem);
        decimal pricedItem = prices[itemPosition];
        return pricedItem;
    }
    catch (Exception ex)
    {
        return null;
    }
}

static double GetAverage(double[] scores)
{
    double sum = 0;
    double average = 0;
    double count = 0;
    foreach (double score in scores)
    {
        if (score != null && score != 0)
        {
            sum = sum + score;
            count++;
        }
        else
        {
            break;
        }
    }
    average = sum / count;
    return average;
}
static double GetMedian(double[] scores)
{
    double median = 0;
    int sizeScores = 0;
    Array.Sort(scores);
    Array.Reverse(scores);
    foreach (double score in scores)
    {
        if (score > 0)
        {
            sizeScores++;
        }
    }
    double[] workingScores = new double[sizeScores];
    for (int cntr = 0; cntr < sizeScores; cntr++)
    {
        workingScores[cntr] = scores[cntr];
    }
    int mid = sizeScores / 2;

    if (sizeScores % 2 != 0)
    {
        median = workingScores[mid];
    }
    else
    {
        double value1 = workingScores[mid];
        double value2 = workingScores[mid - 1];
        median = (value1 + value2) / 2;
    }

    return median;

}
static double GetLargest(double[] scores)
{
    double largest = 0;
    Array.Sort(scores);
    /* Console.WriteLine("GetLargest function {0} , {1}, {2}, {3}, {4}, {5}, {6}", scores[0], scores[1], scores[2], scores[3], scores[4], scores[5], scores[6]);*/
    largest = scores[6];
    return largest;
}
static double GetSmallest(double[] scores)
{
    double smallest = 0;
    Array.Sort(scores);
    Array.Reverse(scores);
    /*scores.Reverse<double>();*/
    foreach (double score in scores)
    {
        if (score == null || score == 0)
        {
            break;
        }
        else
        {
            smallest = score;
        }
    }
    /* Console.WriteLine("GetSmallest function {0} , {1}, {2}, {3}, {4}, {5}, {6}", scores[0], scores[1], scores[2], scores[3], scores[4], scores[5], scores[6]);*/
    return smallest;
}