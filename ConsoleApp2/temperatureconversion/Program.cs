﻿// See https://aka.ms/new-console-template for more information

double farenheitTemperature = 0; 
double celsiusTemperature = 0;
Console.Write("Enter a Farenheit temp to convert to Celsius: ");
string inputStringFarenheit = Console.ReadLine();
farenheitTemperature = Convert.ToDouble(inputStringFarenheit);
celsiusTemperature = (farenheitTemperature - 32.0) * 5 / 9 ;
Console.WriteLine("Farenheit temperature: {0}\x00b0 = Celsius: {1:F2}\x00b0", farenheitTemperature,celsiusTemperature);

