﻿namespace ToDoTracker
{
    public class Program
    {
        public static List<ToDoTask> ToDoList = new List<ToDoTask>()
        {
            new ToDoTask("001", "Complete project report", 3, "John", new DateTime(2023, 4, 15)),
            new ToDoTask("002", "Prepare presentation slides", 2, "Jane", new DateTime(2023, 4, 20)),
            new ToDoTask("003", "Attend meeting with client", 1, "Tom", new DateTime(2023, 4, 25)),
            new ToDoTask("004", "Review team member's code", 4, "Peter", new DateTime(2023, 4, 30)),
            new ToDoTask("005", "Submit expense report", 2, "Mary", new DateTime(2023, 5, 5))
        };
        static void Main(string[] args)
        {
            string command = "";

            while (command != "5")
            {
                Console.WriteLine("1: Add a task");
                Console.WriteLine("2: Remove a task");
                Console.WriteLine("3: List tasks by due date");
                Console.WriteLine("4: List tasks by difficulty");
                Console.WriteLine("5: Exit");
                Console.Write("Your Choice: ");
                command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        addATask();
                        break;
                    case "2":
                        removeATask();
                        break;
                    case "3":
                        listTasksByDueDate();
                        break;
                    case "4":
                        listTasksByDifficulty();
                        break;
                    case "5":
                        break;

                    default: break;
                }
            }
        }
        public static void removeATask()
        {
            Console.Write("Enter the task ID to remove: ");
            string taskID = Console.ReadLine();

            // Find the task to remove based on its TaskID
            ToDoTask taskToRemove = ToDoList.FirstOrDefault(t => t.TaskID == taskID);

            if (taskToRemove != null)
            {
                // Remove the task from the list
                ToDoList.Remove(taskToRemove);

                Console.WriteLine($"Task with ID {taskID} removed successfully.");
            }
            else
            {
                Console.WriteLine($"Task with ID {taskID} not found in the list.");
            }
        }

        public static void listTasksByDueDate()
        {
            Console.WriteLine("Tasks by due date:");

            // Sort tasks by due date in ascending order
            List<ToDoTask> tasksByDueDate = ToDoList.OrderBy(t => t.DueDate).ToList();

            // Print each task in the sorted list
            foreach (ToDoTask task in tasksByDueDate)
            {
                Console.WriteLine($"Task ID: {task.TaskID}, Description: {task.Description}, Difficulty: {task.Difficulty}, Assigned to Name: {task.Assigned_To_Name}, Due Date: {task.DueDate.ToShortDateString()}");
            }
        }

        public static void listTasksByDifficulty()
        {
            Console.WriteLine("Tasks by difficulty in ascending order:");

            // Sort tasks by difficulty in ascending order
            List<ToDoTask> tasksByDifficulty = ToDoList.OrderBy(t => t.Difficulty).ToList();

            // Print each task in the sorted list
            foreach (ToDoTask task in tasksByDifficulty)
            {
                Console.WriteLine($"Task ID: {task.TaskID}, Description: {task.Description}, Difficulty: {task.Difficulty}, Assigned to Name: {task.Assigned_To_Name}, Due Date: {task.DueDate.ToShortDateString()}");
            }
        }

        public static void addATask()
        {
            Console.WriteLine("Enter task details:");

            // Prompt user for input and validate
            string taskID;
            do
            {
                Console.Write("Task ID (must be unique): ");
                taskID = Console.ReadLine();
                if (ToDoList.Any(t => t.TaskID == taskID))
                {
                    Console.WriteLine("Task ID already exists. Please enter a unique task ID.");
                }
            } while (ToDoList.Any(t => t.TaskID == taskID));

            Console.Write("Description: ");
            string description = Console.ReadLine();

            int difficulty;
            do
            {
                Console.Write("Difficulty (1-5): ");
                if (!int.TryParse(Console.ReadLine(), out difficulty) || difficulty < 1 || difficulty > 5)
                {
                    Console.WriteLine("Invalid input. Difficulty must be a number between 1 and 5.");
                }
            } while (difficulty < 1 || difficulty > 5);

            Console.Write("Assigned to name: ");
            string assignedToName = Console.ReadLine();

            DateTime dueDate;
            do
            {
                Console.Write("Due date (yyyy-mm-dd): ");
                if (!DateTime.TryParse(Console.ReadLine(), out dueDate))
                {
                    Console.WriteLine("Invalid input. Due date must be in the format yyyy-mm-dd.");
                }
                else if (dueDate < DateTime.Today)
                {
                    Console.WriteLine("Invalid input. Due date must be in the future.");
                }
            } while (dueDate < DateTime.Today);

            // Create new task object and add it to the list
            ToDoTask newTask = new ToDoTask(taskID, description, difficulty, assignedToName, dueDate);
            ToDoList.Add(newTask);

            Console.WriteLine("Task added successfully.");
        }

    }
}
