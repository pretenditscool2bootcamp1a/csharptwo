﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesExample
{
    public class Lion : Animal, ICarnivore
    {
        public override string Name { get; set; }
        public override int Age { get; set; }
        public  string habitat { get ; set; } 

        public string Hunt()
        {
            return "The lion stalks its prey and pounces to catch it.";
        }

        public override string MakeSound()
        {
            return "Roar!!";
        }
    }
}
