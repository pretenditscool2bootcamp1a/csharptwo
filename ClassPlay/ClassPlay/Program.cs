﻿using ClassPlay;
public class Program {

    static void Main(string[] args)
    {


        Employee emp2 = new Employee("Wendy", "williams", 4);
        /*DisplayEmployee(emp2);*/
        emp2.Display();

        Employee emp3 = new Employee("Seamus", "O'Shaunessy", 6);
        emp3.Display();


        Employee emp1 = new Employee("steve", "early", 1, 1984, "operations", "Senior Engineer", 200000);
        /*       {
                   FirstName = "steve",
                   LastName = "early",
                   EmployeeId = 1,
                   Department = "operations",
                   HireYear = 1984,
                   JobTitle = "Senior Engineer",
                   Salary = 200000
               };*/
        emp1.Display();


        emp3.Promote("Hiring Manager", 50000M);
        emp3.Display();
    }
}

 
 /*   public static void DisplayEmployee (Employee employee1) 
    {
        Console.WriteLine (employee1.ToString());
    }
}*/