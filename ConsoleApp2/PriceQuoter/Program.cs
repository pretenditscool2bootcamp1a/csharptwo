﻿string inputStringProductCode = "";
double perUnitPrice = 0;
int quantity = 0;
string inputQuantity = "";
string command = "";

while (command != "QUIT")
{
    inputStringProductCode = "";
    perUnitPrice = 0;
    quantity = 0;
    inputQuantity = "";
    command = "";
    Console.WriteLine("Commands -->");
    Console.WriteLine(" GET-QUOTE : Place an order");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "GET-QUOTE")
    {
        Console.Write("Enter a product code ");
       
        inputStringProductCode = Console.ReadLine();
        Console.Write("Enter a quantity ");
        inputQuantity = Console.ReadLine();
        quantity = Convert.ToInt16(inputQuantity);
        // do something (example displays "ordering…")
        switch (quantity)
        {
            case > 0 and < 25:
                {
                    if (inputStringProductCode == "BG-127")
                    {
                        perUnitPrice = 18.99;
                    }
                    else if (inputStringProductCode == "WRTR-28")
                    {
                        perUnitPrice = 125.00;
                    }
                    else if (inputStringProductCode == "GUAC-8")
                    {
                        perUnitPrice = 8.99;
                    }
                    break;
                }
            case >= 25 and <= 50:
                {
                    if (inputStringProductCode == "BG-127")
                    {
                        perUnitPrice = 17.00;
                    }
                    else if (inputStringProductCode == "WRTR-28")
                    {
                        perUnitPrice = 113.75;
                    }
                    else if (inputStringProductCode == "GUAC-8")
                    {
                        perUnitPrice = 8.99;
                    }
                    break;
                }
            case >= 51:
                {
                    if (inputStringProductCode == "BG-127")
                    {
                        perUnitPrice = 14.49;
                    }
                    else if (inputStringProductCode == "WRTR-28")
                    {
                        perUnitPrice = 99.99;
                    }
                    else if (inputStringProductCode == "GUAC-8")
                    {
                        perUnitPrice = 7.49;
                    }
                    break;
                }
            default:
                {
                    perUnitPrice = 0;
                    break;
                }


        }
        Console.WriteLine("Product Code {0} , Quantity {1} , per unit price {2:C} , Total Charge {3:C}", inputStringProductCode, quantity, perUnitPrice, (perUnitPrice * quantity));
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}








