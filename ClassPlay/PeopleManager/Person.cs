﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    public class Person :IComparable<Person>
    {
        public string Name { get; set; }
        public int Age { get; private set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        // constructor 
        public Person(string name, int age, string address, string city, string state, string zipCode)
        {
            Name = name;
            Age = age;
            Address = address;
            City = city;
            State = state;
            ZipCode = zipCode;
        }
        // methods
        public void HaveABirthday()
        {
            this.Age++; 
        }

        public void Move (string address, string city, string state, string zipCode)
        {
            this.City = city;
            this.Address = address;
            this.State = state;
            this.ZipCode = zipCode;
        }

        public virtual void Display ()
        {
            Console.WriteLine("Person name: {0} is {1} years old and lives at :", this.Name, this.Age);
            Console.WriteLine("Address : {0} City: {1} State: {2} Zip: {3}", this.Address, this.City, this.State, this.ZipCode);
        }

        // interface method 
        public int CompareTo(Person other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}
