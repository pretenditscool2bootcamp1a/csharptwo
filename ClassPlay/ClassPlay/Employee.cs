﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPlay
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EmployeeId { get; set; }
        public int HireYear { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public decimal Salary { get; set; }

        public Employee(string firstName, string lastName, int employeeId, int hireYear, string jobTitle, string department, decimal salary)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;
            this.HireYear = hireYear;
            this.JobTitle = jobTitle;
            this.Department = department;
            this.Salary = salary;
        }
        public Employee (string firstName, string lastName, int employeeId) : this()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.EmployeeId = employeeId;
/*
            this.HireYear = DateTime.Now.Year;
            this.JobTitle = "New Hire";
            this.Department = "TBD";
            this.Salary = 15.00M;*/
        }

        public Employee ()
        {
            this.HireYear = DateTime.Now.Year;
            this.JobTitle = "New Hire";
            this.Department = "TBD";
            this.Salary = 15.00M;

        }

        // methods 
        public void Promote (string newJobTitle, decimal newSalary)
        {
            this.JobTitle = newJobTitle;
            this.Salary = newSalary;
        }
        public void Display ()
        {
            Console.WriteLine(ToString ());
        }

        public override string ToString()
        {
            return " Employee.FirstName: " + this.FirstName + " Employee.LastName: " + this.LastName + " Employee.ID: " + this.EmployeeId 
                + " Emplyee.hireYear: " + this.HireYear + " Employee.jobTitle: " + this.JobTitle + " Employee.department: " + this.Department 
                + " Employee.salary: " + this.Salary ;
        }
    }

   
}
