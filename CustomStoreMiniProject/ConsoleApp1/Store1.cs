﻿using System.Collections.Immutable;
using System.Globalization;
using System.Linq.Expressions;

namespace ConsoleApp1
{


    public class Store1
    {
        public static List<InventoryItem> inventory = null ;
        public static StreamWriter logfile = null ;
        static void Main(string[] args)
        {
            
            
            DateTime runTime = DateTime.Now;
            StreamWriter logfile = null;
            StreamReader inputInventoryFile = null;
            
            prepInventory(ref logfile);
            foreach (InventoryItem item in inventory)
            {
                Console.WriteLine("{0} {1} {2} {3}", item.Id, item.Name, item.Description, item.Type );
            }


        } // main 

        static void prepInventory(ref StreamWriter logfile )
        {
            DateTime runTime = DateTime.Now;
            string logFileName = @"C:\Academy\csharp\csharptwo\CustomStoreMiniProject\ConsoleApp1\data\InventoryProcessing.log";
            // open the log file 
            string inventoryFile = @"C:\Academy\csharp\csharptwo\CustomStoreMiniProject\ConsoleApp1\data\intentory1.txt";
            try
            {
                using (logfile = new StreamWriter(logFileName, true))
                {
                    logfile.WriteLine("Loading Inventory file: {0} on {1} at {2}", inventoryFile, runTime.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), runTime.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                    // open the inventory file and load inventory list 
                    try
                    {
                        inventory = ReadInventoryItemsFromFile(inventoryFile);
                        foreach (InventoryItem item in inventory)
                        {
                            Console.WriteLine("test");
                        }
                    }
                    catch (Exception ex)
                    {
                        logfile.WriteLine("Error in loading the inventory file: {0}", ex.Message);
                    }
                }

            }
            catch (Exception ex2)
            {
                logfile.WriteLine(ex2.ToString());
            }
        }

        static List<InventoryItem> ReadInventoryItemsFromFile(string filePath)
        {
            List<InventoryItem> items = new List<InventoryItem>();

            string[] lines = File.ReadAllLines(filePath);
            int lineCounter = 0;
            foreach (string line in lines)
            {
                lineCounter++;
                string[] fields = line.Split('|');
                try
                {
                    if (fields[2] == "PrimaryWeapon")
                    {
                        PrimaryWeapon weapon = new PrimaryWeapon(fields[0], fields[1], fields[2], fields[3], decimal.Parse(fields[4]), int.Parse(fields[5]), fields[6], int.Parse(fields[7]), int.Parse(fields[8]), int.Parse(fields[9]), int.Parse(fields[10]));
                        items.Add(weapon);
                    }
                    else if (fields[2] == "SecondaryWeapon")
                    {
                        SecondaryWeapon weapon = new SecondaryWeapon(fields[0], fields[1], fields[2], fields[3], decimal.Parse(fields[4]), int.Parse(fields[5]), fields[6], int.Parse(fields[7]), int.Parse(fields[8]), int.Parse(fields[9]), int.Parse(fields[10]));
                        items.Add(weapon);
                    }
                    else if (fields[2] == "Melee")
                    {
                        MeleeWeapon weapon = new MeleeWeapon(fields[0], fields[1], fields[2], fields[3], decimal.Parse(fields[4]), int.Parse(fields[5]), fields[6], int.Parse(fields[7]), int.Parse(fields[8]), decimal.Parse(fields[9]), int.Parse(fields[10]));
                        items.Add(weapon);
                    }
                    else if (fields[2] == "Frame")
                    {
                        WarFrame warframe = new WarFrame(fields[0], fields[1], fields[2], fields[3], decimal.Parse(fields[4]), int.Parse(fields[5]), int.Parse(fields[6]), int.Parse(fields[7]), int.Parse(fields[8]), int.Parse(fields[9]), decimal.Parse(fields[10]));
                        items.Add(warframe);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error reading line {0} with exception : {1}", lineCounter, ex.Message);
                }
            }

            return items;
        }
    } // class store1
} // namespace 