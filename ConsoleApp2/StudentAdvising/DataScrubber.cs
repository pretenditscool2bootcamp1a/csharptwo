﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentAdvising
{
    public class DataScrubber
    {
        
        public static void ScrubPhone(ref string phoneNumber)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' ');
        }
        public static void ScrubPhone(ref string phoneNumber, string charToReplace)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' '); 
            string localPhoneNumber = phoneNumber.Replace(charToReplace, "");
            phoneNumber = localPhoneNumber;
        }
        public static void ScrubPhone(ref string phoneNumber, string charToReplace, string charReplacementChar)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' ');
            string localPhoneNumber = phoneNumber.Replace(charToReplace, charReplacementChar);
            phoneNumber = localPhoneNumber;
        }
    }
}
