﻿
namespace LINQPlay
{
    public class LinqPlay
    {
        static void Main(string[] args)
        {
            List<Stock> stockList = new List<Stock>();

            // Adding 10 examples of data for Stock
            stockList.Add(new Stock("AAPL", "Apple Inc.", 135.98M, 100));
            stockList.Add(new Stock("GOOGL", "Alphabet Inc.", 2102.43M, 50));
            stockList.Add(new Stock("AMZN", "Amazon.com Inc.", 3057.64M, 75));
            stockList.Add(new Stock("MSFT", "Microsoft Corporation", 232.42M, 150));
            stockList.Add(new Stock("FB", "Facebook Inc.", 282.14M, 80));
            stockList.Add(new Stock("TSLA", "Tesla Inc.", 670.00M, 25));
            stockList.Add(new Stock("JPM", "JPMorgan Chase & Co.", 156.06M, 200));
            stockList.Add(new Stock("JNJ", "Johnson & Johnson", 161.07M, 120));
            stockList.Add(new Stock("V", "Visa Inc.", 219.11M, 60));
            stockList.Add(new Stock("PG", "Procter & Gamble Co.", 129.94M, 90));

            var cheapStocks = from stock in stockList
                              where stock.SharePrice < 200M
                              select new { stock.TickerSymbol, stock.StocName, stock.SharePrice };
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("Show all stocks where price per share is less than $200: ");
            foreach (var stock in cheapStocks)
            {
                Console.WriteLine($"TickerSymbol: {stock.TickerSymbol}, StockName: {stock.StocName}, SharePrice: {stock.SharePrice:C}");
            }

            var midPriceStocks = stockList.Where(stock => stock.SharePrice >= 200M && stock.SharePrice <= 300M)
                              .Select(stock => new { stock.TickerSymbol, stock.StocName, stock.SharePrice });
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("Show all stocks where price per share is >= to $200 and <= $300: ");
            foreach (var stock in midPriceStocks)
            {
                Console.WriteLine($"TickerSymbol: {stock.TickerSymbol}, StockName: {stock.StocName}, SharePrice: {stock.SharePrice:C}");
            }

        }
    }
}