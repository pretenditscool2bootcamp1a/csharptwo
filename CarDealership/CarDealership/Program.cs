﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks.Sources;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using static CarDealership;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Reflection;

public class CarDealership
{


    static void Main(string[] args)
    {
        string command = "";
       
        while (command != "6")
        {
            
            Console.WriteLine("What do you want to do?");
            Console.WriteLine("1 - Find vehicles that match make/mode");
            Console.WriteLine("2 - Find vehicles that fall within a price range");
            Console.WriteLine("3 - Find vehicles that match a color");
            Console.WriteLine("4 - Show all vehicles");
            Console.WriteLine("5 - Add a vehicle");
            Console.WriteLine("6 - Quit");
            Console.WriteLine("Enter the number for your command: ");
            command = Console.ReadLine();
            switch (command)
            {
                case "1":
                    {
                        findByMakeModel();
                        break;
                    }
                case "2":
                    {
                        findWithinPriceRange();
                        break;
                    }
                case "3":
                    {
                        findByColor();
                        break;
                    }
                case "4":
                    {
                        showAllVehicles();
                        break;
                    }
                case "5":
                    {
                        addVehicle();
                        break;
                    }
                case "6": break;
                default: break;
            }
            
            
        }
    }

    public static void addVehicle()
    {
        string[] inputFields = new string[6];
        Console.WriteLine("----------------------");
        Console.WriteLine("Adding new vehicle");
       
        Console.Write("Enter new vehicle's Make:");
        inputFields[1] = Console.ReadLine();
        
        Console.Write("Enter new vehicle's Model:");
        inputFields[2] = Console.ReadLine();
        Console.Write("Enter new vehicle's Color:");
        inputFields[3] = Console.ReadLine();
        Console.Write("Enter new vehicle's Odometer:");
        inputFields[4] = Console.ReadLine();
        Console.Write("Enter new vehicle's Price:");
        inputFields[5] = Console.ReadLine();
        Console.WriteLine("----------------------");
        for (int i = 1; i < 6; i++)
        {
            if (inputFields[i] == null || inputFields[i] == "")
            {
                Console.WriteLine("All entered vehicle fields must have a value. returning to menu");
                return;
            }
        }
        
        Vehicle newVehicle = new Vehicle()
        {
            Id = -1,
            Make = inputFields[1],
            Model = inputFields[2],
            Color = inputFields[3],
            Odometer = Convert.ToInt32(inputFields[4]),
            Price = Convert.ToDecimal(inputFields[5])
        }
        ;
        bool addedVehicle = addVehicleToFile(newVehicle);
        if (addedVehicle)
        {
            Console.WriteLine("New Vehicle added to inventory");
        }
        else
        {
            Console.WriteLine("New Vehicle failed to add to inventory");
        }
    }

    public static void findByColor()
    {
            string arrayExtents = "";
            string make = "";
            string model = ""; 
            Console.WriteLine("----------------------");
            Console.Write("Enter color to search vehicles by: ");
            string color = Console.ReadLine();
           
            if (color == null || color == "")
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("no color entered, returning to menu");
            }
            List<Vehicle> vehicleInventory = new List<Vehicle>();

            arrayExtents = populateVehicleList(make, model, color, 0M, 0M, ref vehicleInventory);
            if (arrayExtents == null)
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("No Matching Vehicles found. Returning to menu");
                return;
            }
            else
            {
                foreach (Vehicle vehicle in vehicleInventory)
                {
                    Console.WriteLine("----------------------");
                    Console.WriteLine("Vehicle listing: Id: {0} Make: {1} Model: {2} ", vehicle.Id, vehicle.Make, vehicle.Model);
                }
                detailsMenu(ref vehicleInventory);

            }        
    }
    public static void findWithinPriceRange()
    {
        string arrayExtents = "";
        string color = "";
        string make = "";
        string model = "";
        decimal lowRange = 0M;
        decimal highRange = 0M;

        Console.WriteLine("----------------------");
        Console.WriteLine("Find vehicles within price range");
        Console.Write("Enter the lowest end of the price range or hit enter for 0 :  ");
        string lowRangeString = Console.ReadLine();
        Console.Write("Enter the highest end of the price range or hit enter for everything :  ");
        string highRangeString = Console.ReadLine();
        try
        {

            highRange = highRangeString == "" ? 99999999999999.99M : Convert.ToDecimal(highRangeString);
            lowRange = lowRangeString == "" ? 0M : Convert.ToDecimal(lowRangeString);
        }
        catch (Exception ex)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine($"Error: {ex.Message}");
            Console.WriteLine("Returning to main menu");
            return;
        }
        List<Vehicle> vehicleInventory = new List<Vehicle>();

        arrayExtents = populateVehicleList(make, model, color, lowRange, highRange, ref vehicleInventory);
        if (arrayExtents == null)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("No Matching Vehicles found. Returning to menu");
            return;
        }
        else
        {
            foreach (Vehicle vehicle in vehicleInventory)
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("Vehicle listing: Id: {0} Make: {1} Model: {2} ", vehicle.Id, vehicle.Make, vehicle.Model);
            }
            detailsMenu(ref vehicleInventory);

        }

    }
    public static bool addVehicleToFile(Vehicle addedVehicle)
    {
        bool successState = false;
        string inventoryFileName = @"C:\Academy\csharp\datafiles\carDealershipInventory.txt";
        string logFileName = @"C:\Academy\csharp\datafiles\dealershipApp.log";
        DateTime whatTimeItIs = DateTime.Now;
        int vehicleCnt = File.ReadLines(inventoryFileName).Count();
        vehicleCnt++;
        try
        {
            using (StreamWriter logfile = new StreamWriter(logFileName, true))
            {
                logfile.WriteLine("Attempting to Add vehicle to inventory file: {0} on {1} at {2}", inventoryFileName, whatTimeItIs.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), whatTimeItIs.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                using (StreamWriter inputFile = new StreamWriter(inventoryFileName, true))
                {
                    /*inputFile.WriteLine();*/
                    inputFile.WriteLine("{0},{1},{2},{3},{4},{5}",vehicleCnt,addedVehicle.Make,addedVehicle.Model,addedVehicle.Color,addedVehicle.Odometer,addedVehicle.Price);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }
        successState = true;    
        return successState;
    }
    public static string populateVehicleList (string make, string model, string color, decimal lowRange, decimal highRange, ref List<Vehicle> vehicleInventory)
    {
        string arrayExtents = "0,0";
        string inventoryFileName = @"C:\Academy\csharp\datafiles\carDealershipInventory.txt";
        string logFileName = @"C:\Academy\csharp\datafiles\dealershipApp.log";
        int inventoryCount = 0;
        int numberOfFieldsPer = 0;
        string localMake = "";
        string localModel = "";
        string mightBeMakeMightBeModel = "";
        DateTime whatTimeItIs = DateTime.Now;
        if (highRange != null && highRange == 0)
        {
            highRange = 9999999999999999999.99M;
        }
        try
        {
            using (StreamWriter logfile = new StreamWriter(logFileName, true))
            {
                logfile.WriteLine("Processing file: {0} on {1} at {2}", inventoryFileName, whatTimeItIs.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), whatTimeItIs.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                using (StreamReader inputFile = new StreamReader(inventoryFileName))
                {
                    while (inputFile.EndOfStream != true)
                    {
                        string text = inputFile.ReadLine();
                        // do the things to the lines
                        string[] inputFields = text.Split(",");
                        numberOfFieldsPer = inputFields.Length;

                        inventoryCount++;
                        /*Console.WriteLine("{3} Vehicle number: {0} Make: {1} Model: {2}", inputFields[0], inputFields[1], inputFields[2], InventoryCount );*/
                        if (lowRange != 0 && highRange != 0)
                        {
                            if (Convert.ToDecimal(inputFields[5]) >= lowRange && Convert.ToDecimal(inputFields[5]) <= highRange) 
                            {
                                vehicleInventory.Add
                                                           (new Vehicle()
                                                           {
                                                               Id = Convert.ToInt16(inputFields[0]) /*!= null ? Convert.ToInt16(inputFields[0]) : -1*/ ,
                                                               Make = inputFields[1] /*!= null ? inputFields[1] : "Error: no input field value"*/,
                                                               Model = inputFields[2] /*!= null ? inputFields[2] : "Error: no input field value"*/,
                                                               Color = inputFields[3] /*!= null ? inputFields[3] : "Error: no input field value"*/,
                                                               Odometer = Convert.ToInt32(inputFields[4]) /*!= null ? Convert.ToInt32(inputFields[4]) : -1*/,
                                                               Price = Convert.ToDecimal(inputFields[5]) /*!= null ? Convert.ToDecimal(inputFields[5]) : -1M*/
                                                           }
                                                           );
                            }
                        }   
                        else if (
                                (make != "" && make == inputFields[1])
                                || 
                                (model != "" && model == inputFields[2])
                                ||
                                (make == "" && model == "" && (color != "" && color == inputFields[3] ))
                                ||
                                (make == "" && model == "" && color == "")
                            )
                        {
                            vehicleInventory.Add
                            (new Vehicle()
                            {
                                Id = Convert.ToInt16(inputFields[0]) /*!= null ? Convert.ToInt16(inputFields[0]) : -1*/ ,
                                Make = inputFields[1] /*!= null ? inputFields[1] : "Error: no input field value"*/,
                                Model = inputFields[2] /*!= null ? inputFields[2] : "Error: no input field value"*/,
                                Color = inputFields[3] /*!= null ? inputFields[3] : "Error: no input field value"*/,
                                Odometer = Convert.ToInt32(inputFields[4]) /*!= null ? Convert.ToInt32(inputFields[4]) : -1*/,
                                Price = Convert.ToDecimal(inputFields[5]) /*!= null ? Convert.ToDecimal(inputFields[5]) : -1M*/
                            }
                            );
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }

        return arrayExtents;
    }
    public static void findByMakeModel()
    {

        string arrayExtents = "";
        string color = "";
        Console.WriteLine("----------------------");
        Console.Write("Enter Make or just hit enter to skip and just enter model: ");
        string make = Console.ReadLine();
        Console.WriteLine("Enter Model or just hit enter to skip");
        string model = Console.ReadLine();
        if (make == null && model == null)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("no make or model entered, returning to menu");
        }
        List<Vehicle> vehicleInventory = new List<Vehicle>();
        
        arrayExtents = populateVehicleList(make, model, color, 0M, 0M , ref vehicleInventory);
        if (arrayExtents == null)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("No Matching Vehicles found. Returning to menu");
            return;
        }
        else
        {
            foreach (Vehicle vehicle in vehicleInventory)
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("Vehicle listing: Id: {0} Make: {1} Model: {2} ", vehicle.Id, vehicle.Make, vehicle.Model);
            }
            detailsMenu(ref vehicleInventory); 

        }

    }
    public static void detailsMenu(ref List<Vehicle> vehicleInventory)
    {
        string command, subCommand;
        Console.WriteLine("----------------------");
        Console.WriteLine("Commands -->");
        Console.WriteLine(" 1 - Show detailed information about vehicles");
        Console.WriteLine(" 2 - Return to previous menu");
        command = Console.ReadLine();

        if (command == "1")
        {

            Console.Write("Enter the vehicle Id to see its details : ");
            string vehicleIdPicked = Console.ReadLine();
            Vehicle vehiclePicked = displayVehicleDetails(vehicleIdPicked, vehicleInventory);
            Console.WriteLine("----------------------");
            Console.WriteLine("SubCommands =>");
            Console.WriteLine("1 - Purchase: purchase vehicle");
            Console.WriteLine("2 -Return: return to the main menu");
            subCommand = Console.ReadLine();
            while (subCommand != "2")
            {
                if (subCommand == "1")
                {
                    prepareInvoice(vehiclePicked);
                    subCommand = "2";
                }
                else if (subCommand != "2")
                {
                    Console.WriteLine("**Error: unrecognized command");
                    return;
                }
            }
        }
        else if (command == "2")
        {
            return; 
        }
        else if (command != "QUIT")
        {
            Console.WriteLine("**Error: unrecognized command");
            return;
        }
    }
    public static void showAllVehicles()
    {
        string arrayExtents = "";
        string color = "";
        string make = "";
        string model = "";

        Console.WriteLine("----------------------");
  
        List<Vehicle> vehicleInventory = new List<Vehicle>();

        arrayExtents = populateVehicleList(make, model, color, 0M, 0M, ref vehicleInventory);
        if (arrayExtents == null)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("No Matching Vehicles found. Returning to menu");
            return;
        }
        else
        {
            foreach (Vehicle vehicle in vehicleInventory)
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("Vehicle listing: Id: {0} Make: {1} Model: {2} ", vehicle.Id, vehicle.Make, vehicle.Model);
            }
            detailsMenu(ref vehicleInventory);

        }

    }
    
    public static bool removeFromInventory(Vehicle vehicleToRemove)
    {
        bool successState = false;
        string arrayExtents = "";
        
        List<Vehicle> vehicleInventory = new List<Vehicle>();

        arrayExtents = populateVehicleList("", "", "", 0M, 0M, ref vehicleInventory);
        vehicleInventory.Remove(new Vehicle() { Id = vehicleToRemove.Id});
        successState = writeNewInventory(vehicleInventory);
        return successState;

    }

    public static bool writeNewInventory(List<Vehicle> vehicleInventory) 
    {
        bool successState = false;
        string inventoryFileName = @"C:\Academy\csharp\datafiles\carDealershipInventory.txt";
        string newInventoryFileName = @"C:\Academy\csharp\datafiles\carDealershipInventory.txt" ;
        string logFileName = @"C:\Academy\csharp\datafiles\dealershipApp.log";
        DateTime whatTimeItIs = DateTime.Now;
        try
        {
            using (StreamWriter logfile = new StreamWriter(logFileName, true))
            {
                logfile.WriteLine("Writing new inventory file: {0} on {1} at {2}", newInventoryFileName, whatTimeItIs.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), whatTimeItIs.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                using (StreamWriter newInputFile = new StreamWriter(newInventoryFileName))
                {
                    foreach (Vehicle vehicle in vehicleInventory)
                    {
                        newInputFile.WriteLine("{0},{1},{2},{3},{4},{5}", vehicle.Id, vehicle.Make, vehicle.Model, vehicle.Color, vehicle.Odometer, vehicle.Price);
                    }
                }
                File.Copy(newInventoryFileName, inventoryFileName, true);
            }
            successState = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }
        
        return successState;
    }
    public static void prepareInvoice(Vehicle vehicle)
    {
        bool removeSuccessState = false;
        Console.Write("Enter name: ");
        string purchaserName = Console.ReadLine();
        createInvoice(purchaserName, vehicle);
        removeSuccessState = removeFromInventory(vehicle);
    }

    public static void createInvoice (string purchaserName, Vehicle vehicle)
    {
        DateTime whatTimeItIs = DateTime.Now;
        string invoiceFileName = @"C:\Academy\csharp\datafiles\invoice-" + purchaserName + whatTimeItIs.Year + whatTimeItIs.Month + whatTimeItIs.Day + "-" + whatTimeItIs.Hour + whatTimeItIs.Minute + whatTimeItIs.Second + ".txt";
        try
        {
            using (StreamWriter logfile = new StreamWriter(invoiceFileName, false))
            {
                logfile.WriteLine("Invoice file: for purchaser : {0} on {1} at {2}", purchaserName, whatTimeItIs.ToString("d", CultureInfo.CreateSpecificCulture("en-US")), whatTimeItIs.ToString("t", CultureInfo.CreateSpecificCulture("en-us")));
                logfile.WriteLine("--------------------------------------------");
                logfile.WriteLine(vehicle.ToString());                  
               
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error opening file: {ex.Message}");
        }
    } 
    public static Vehicle displayVehicleDetails(string ID, List<Vehicle> vehicleInventory)
    {
        int intId = 0;
        intId = Convert.ToInt32(((string)ID));
        Vehicle detailsvehicle = vehicleInventory.Find(vehicle => vehicle.Id == intId);
        Console.WriteLine("{0}", detailsvehicle.ToString());
        return detailsvehicle;
    }

    public class Vehicle : IEquatable<Vehicle>
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public int Odometer { get; set; }
        public decimal Price { get; set; }

        public override string ToString()
        {
            return "Id: " + Id + "  Make: " + Make + " Model: " + Model + " Color: " + Color + " Odometer: " + Odometer + " Price: " + Price ;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Vehicle objAsPart = obj as Vehicle;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }
        public override int GetHashCode()
        {
            return Id;
        }
        public bool Equals(Vehicle other)
        {
            if (other == null) return false;
            return (this.Id.Equals(other.Id));
        }
        // Should also override == and != operators.
    }
}

