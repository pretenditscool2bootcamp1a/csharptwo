﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleManager
{
    internal class Worker : Person
    {
        public string JobTitle { get; set; }
        public decimal Salary { get; set; }

        public Worker(string name, int age, string address, string city, string state, string zipCode, string jobTitle, decimal salary) : base(name, age, address, city, state, zipCode)
        {
            JobTitle = jobTitle;
            Salary = salary;
        }

        public void Display()
        {
            Console.WriteLine("This is a worker. Their Jobtitle is {0} and their salary is {1}", this.JobTitle, this.Salary);
            Console.WriteLine("Person name: {0} is {1} years old and lives at :", this.Name, this.Age);
            Console.WriteLine("Address : {0} City: {1} State: {2} Zip: {3}",this.Address, this.City, this.State, this.ZipCode);
        }
    }
}
