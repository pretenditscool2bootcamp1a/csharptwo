﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesExample
{
    public abstract class Animal
    {
        public abstract string Name { get; set; }
        public abstract int Age { get; set; }

        //methods
        public abstract string MakeSound();

        public void DisplayInfo()
        {
            Console.WriteLine("Name : {0}  Age: {1}", this.Name, this.Age);
        }
    }
}
