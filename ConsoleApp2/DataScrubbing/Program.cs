﻿string command = "";
double returnValueDbl = 0.0;
string returnValueString = "";

while (command != "QUIT")
{
    returnValueDbl = 0.0;
    command = "";
    Console.WriteLine("Commands -->");
    Console.WriteLine(" Scrub : Scrub leading and Trailing spaces from a phone number");
    Console.WriteLine(" CtoF : Convert a Celsius temp to Farenheit");
    Console.WriteLine(" FtoC : Convert a Farenheit temp to Celsius");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();
    if (command == "CtoF")
    {
        try
        {
            Console.Write("Enter a Celsius temp to convert to Farenheit: ");
            string inputStringCelsius = Console.ReadLine();
            returnValueDbl = convertCtoF(inputStringCelsius);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", returnValueDbl, inputStringCelsius);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "FtoC")
    {
        try
        {
            Console.Write("Enter a Farenheit temp to convert to Celsius: ");
            string inputStringFarenheit = Console.ReadLine();
            returnValueDbl = convertFtoC(inputStringFarenheit);
            Console.WriteLine("Farenheit temperature: {0:F2}\x00b0 = Celsius: {1:F2}\x00b0", inputStringFarenheit, returnValueDbl);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command == "Scrub")
    {
        try
        {
            Console.Write("Enter a Phone Number to Scrub: ");
            string PhoneNumber = Console.ReadLine();
            ScrubPhone(ref PhoneNumber);
            Console.WriteLine("Scrubbed Phone Number: {0} .", PhoneNumber);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine("**Error: unrecognized command");
    }
}

static double convertFtoC(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        farenheitTemperature = Convert.ToDouble(args);
        celsiusTemperature = (farenheitTemperature - 32.0) * 5 / 9;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine(" convertFtoC function: Failed to convert string argument of farenheit temp to double");
    }
    return celsiusTemperature;
}

static double convertCtoF(string args)
{
    double farenheitTemperature = 0;
    double celsiusTemperature = 0;
    try
    {
        celsiusTemperature = Convert.ToDouble(args);
        farenheitTemperature = ((celsiusTemperature * 9.0) / 5) + 32;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine("convertCtoF function: Failed to convert string argument of celsius temp to double");
    }
    return farenheitTemperature;
}

static void ScrubPhone(ref string phoneNumber)
{
    phoneNumber = phoneNumber.TrimStart(' ');
    phoneNumber = phoneNumber.TrimEnd(' ');
}