﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminationDates
{
    public class NameParsing
    {
        public static int[,] CreateArray()
        {
            int[,] table = new int[10, 5];
            return table;
        }
        public static void GetName(out string fullName, out string firstName, out string lastName, out string middleName)
        {
            Console.Write("Enter a full name: ");
            fullName = Console.ReadLine();
            fullName = fullName.ToUpper();
            firstName = fullName.Substring(0, fullName.IndexOf(" "));
            
            lastName = fullName.Substring(fullName.LastIndexOf(" "));
            if (fullName.IndexOf(" ") == fullName.LastIndexOf(" "))
            {
                middleName = "";
            }
            else 
            {
                middleName = fullName.Substring(fullName.IndexOf(" ") + 1, fullName.LastIndexOf(" ") - fullName.IndexOf(" "));
            };
 
        }
        public static void ScrubPhone(ref string phoneNumber)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' ');
        }
        public static void ScrubPhone(ref string phoneNumber, string charToReplace)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' '); 
            string localPhoneNumber = phoneNumber.Replace(charToReplace, "");
            phoneNumber = localPhoneNumber;
        }
        public static void ScrubPhone(ref string phoneNumber, string charToReplace, string charReplacementChar)
        {
            phoneNumber = phoneNumber.TrimStart(' ');
            phoneNumber = phoneNumber.TrimEnd(' ');
            string localPhoneNumber = phoneNumber.Replace(charToReplace, charReplacementChar);
            phoneNumber = localPhoneNumber;
        }
    }
}
