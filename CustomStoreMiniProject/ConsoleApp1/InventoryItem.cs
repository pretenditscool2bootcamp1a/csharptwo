﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class InventoryItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(string id, string name, string type, string description, decimal price, int quantity)
        {
            Id = id;
            Name = name;
            Type = type;
            Description = description;
            Price = price;
            Quantity = quantity;
        }
    }

    public class PrimaryWeapon : InventoryItem
    {
        string DamageType { get; set; }
        int Range { get; set; }
        int MagazineCapacity { get; set; }
        int Damage { get; set; }
        int FireRate { get; set; }

        public PrimaryWeapon(string id, string name, string type, string description, decimal price, int quantity, string damagetype, int range, int magazineCapacity, int damage, int fireRate) : base(id, name, type, description, price, quantity)
        {
            DamageType = damagetype;
            Range = range;
            MagazineCapacity = magazineCapacity;
            Damage = damage;
            FireRate = fireRate;
        }
    }

    public class SecondaryWeapon : InventoryItem
    {
        string DamageType { get; set; }
        int Range { get; set; }
        int MagazineCapacity { get; set; }
        int Damage { get; set; }
        int FireRate { get; set; }
        public SecondaryWeapon(string id, string name, string type, string description, decimal price, int quantity, string damagetype, int range, int magazineCapacity, int damage, int fireRate) : base(id, name, type, description, price, quantity)
        {
            DamageType = damagetype;
            Range = range;
            MagazineCapacity = magazineCapacity;
            Damage = damage;
            FireRate = fireRate;
        }
    }

    public class MeleeWeapon : InventoryItem
    {
        string DamageType { get; set; }
        int Range { get; set; }
        int AttackSpeed { get; set; }
        decimal BlockDamagePercentage { get; set; }
        int BlockingAngle { get; set; }
        public MeleeWeapon(string id, string name, string type, string description, decimal price, int quantity, string damageType, int range, int attackSpeed, decimal blockDamagePercentage, int blockingAngle) : base(id, name, type, description, price, quantity)
        {
            DamageType = damageType;
            Range = range;
            AttackSpeed = attackSpeed;
            BlockDamagePercentage = blockDamagePercentage;
            BlockingAngle = blockingAngle;
        }
    }

    public class WarFrame : InventoryItem
    {
        int Shields { get; set; }
        int Armor { get; set; }
        int Health { get; set; }
        int Energy { get; set; }
        decimal Speed { get; set; }

       public WarFrame (string id, string name, string type, string description, decimal price, int quantity, int shields, int armor, int health, int energy, decimal speed) : base(id, name, type, description, price, quantity)
        {
            Shields = shields;
            Armor = armor;
            Health = health;
            Energy = energy;
            Speed = speed;
        }
    }

}
