﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AssetManager
{
    public class Asset
    {
        public string AssetId { get; set; }
        public string Description { get; set; }
        public string DateAcquired { get; set; }
        public decimal OriginalCost { get; set; }

        public Asset(string assetId, string description, string dateAcquired, decimal originalCost)
        {
            this.AssetId = assetId;
            this.Description = description;
            this.DateAcquired = dateAcquired;   
            this.OriginalCost = originalCost;   
        }

        // methods 
        public virtual decimal GetValue()
        {
            return this.OriginalCost;
        }
    }
    public class Stock : Asset
    {
        public string StockTicker { get; set; }
        public decimal CurrentSharePrice { get; set; }
        public int NumberOfShares { get; set; }

        public Stock (string assetId, string description, string dateAcquired, decimal originalCost, string stockTicker, decimal currentSharePrice, int numberOfShares) : base(assetId, description, dateAcquired,originalCost)
        {
            StockTicker = stockTicker;
            CurrentSharePrice = currentSharePrice;
            NumberOfShares = numberOfShares;
        }

        // methods
        public override decimal GetValue()
        {
            return this.NumberOfShares * this.CurrentSharePrice ;
        }
    }
    public class Car : Asset
    {
        public int ModelYear;
        public int OdometerReading;
        public Car(string assetId, string description, string dateAcquired, decimal originalCost, int modelYear, int odometerReading) : base(assetId, description, dateAcquired, originalCost)
        {
            ModelYear = modelYear;
            OdometerReading = odometerReading;
        }

        //methods 
        public override decimal GetValue()
        {
            decimal carValue = 0;
            int carAge = DateTime.Now.Year - this.ModelYear;
            decimal depreciation = 0;
            if ( carAge < 7)
            {
                depreciation = (this.OdometerReading / 5000M * .02M);
                Int32 depreciationCap = Decimal.Compare(depreciation, .9M);
                if (depreciationCap < 0)
                {
                    depreciation = .9M;
                }
                
            }
            else
            {
                if (this.OdometerReading < 100000)
                {
                    depreciation = .7M;
                }
                else
                {
                    depreciation = .9M;
                }
            }

            carValue = depreciation * this.OriginalCost;
            return carValue;
        }
    }
    
}
