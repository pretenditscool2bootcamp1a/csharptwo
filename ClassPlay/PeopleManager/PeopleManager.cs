﻿using PeopleManager;
using System.Collections.Generic;

namespace PeopleManager
{
    public class PeopleManager
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>();
            people.Add(new Person("Ian", 17, "133 Elm st.", "RockRidge", "ME", "53322"));
            people.Add(new Person("James", 57,"41 Burroughs st.", "Detroit", "MI", "48202"));
            people.Add(new Person("Mile", 33, "1 Woodward Ave.", "Detroit", "MI", "48202"));
            people.Add(new Worker("Mark", 47, "555 Main st.", "Southfield", "MI", "48344", "Software Engineer", 100000M));
            people.Add(new Person("Siddalee", 16, "423 Penny Ln.", "Manchester", "CA", "76454"));
            people.Add(new Person("Pursalane", 13, "666 MockingBird Ln.", "Hollywood", "CA", "90210"));
            people.Add(new Person("Jebediah", 65, "round behind the waffleshack", "Mudwump", "SC", "21332"));
            people.Add(new Worker("Steve", 53, "33 Oak st.", "Ferndale", "MI", "48342", "Personal Trainer", 65000M));
            people.Add(new Worker("Joe", 44, "433 Elm st.", "Ferndale", "MI", "48342", "Junior Spelunker", 33000M));


            // people.Sort(new AscendingAgeSorter());
            foreach (Person person in people)
            {
                person.Display() ;
            }
            Console.WriteLine("people[1] moves");
            people[1].Move("142 Main st.", "Ypsilanti", "MI", "48230");
            people[1].Display();

            Console.WriteLine("Last person in list has a birthday");
            people[people.Count - 1].HaveABirthday();
            people[people.Count - 1].Display();
            Console.Write("list sorted ascending by name");
            people.Sort();
            foreach (Person person in people)
            {
                person.Display();
            }
            Console.WriteLine("list sorted descending by age");
            people.Sort(new DescendingAgeSorter());
            foreach (Person person in people)
            {
                person.Display();
            }

            Console.WriteLine("list sorted ascending by age");
            people.Sort(new AscendingAgeSorter());
            foreach (Person person in people)
            {
                person.Display();
            }

            Console.WriteLine("list sorted descending by zipcode");
            people.Sort(new DescendingZipSorter());
            foreach (Person person in people)
            {
                person.Display();
            }

            Console.WriteLine("list sorted ascending by state by city");
            people.Sort(new AscendingStateCity());
            foreach (Person person in people)
            {
                person.Display();
            }

        }
    }
}
