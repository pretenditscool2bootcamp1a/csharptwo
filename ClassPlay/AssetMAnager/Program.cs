﻿
using AssetManager;
namespace ClassPlay
{
    public class Program
    {

        static void Main(string[] args)
        {
            Asset[] exAssets =
                {
            new Stock("Microsoft", "Microsoft Shares", "Jan, 2021", 5000, "MSF", 1.76M, 5000),
            new Stock("Apple", "Apple Shares", "Jan, 2022", 6000, "APL", 5.22M, 10000),
            new Stock("Alphabet", "Aplhabet Shares", "Dec 2020", 8000, "ALPH", 5.11M, 6000),
            new Car("Car1", "VW Tiguan", "April, 2018", 15000, 2016, 40000),
            new Car("Car2", "VW Jetta", "April, 2010", 13000, 2008, 85000),
            new Car("Car3", "Geo Storm", "Jan, 2022", 17000, 2022, 10000)
            };
            
            foreach (Asset asset in exAssets)
            {
                Console.WriteLine("Description: {0} , Date acquired {1} , Value {2}", asset.Description, asset.DateAcquired, asset.GetValue());
            }

            Car thisIsACar = null;
            foreach (Asset thisAsset in exAssets)
            {
                if (thisAsset is Car)
                {
                    thisIsACar = (Car)thisAsset;
                    Console.WriteLine("Model year {0} with odometer: {1} current value {2}", thisIsACar.ModelYear, thisIsACar.OdometerReading, thisIsACar.GetValue());
                }
            }
            
            Stock thisIsAStock = null;
            foreach (Asset thisAsset in exAssets)
            {
                if (thisAsset is Stock)
                {
                    thisIsAStock = (Stock)thisAsset;
                    Console.WriteLine("Ticker Symbol: {0} , current stock price: {1} , number of shares held {2}", 
                            thisIsAStock.StockTicker, thisIsAStock.CurrentSharePrice, thisIsAStock.NumberOfShares);
                }
            }
        }
    }
}

