﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoTracker
{
    public class AscendingTasksByDifficulty: IComparer<ToDoTask>
    {
        public int Compare(ToDoTask x, ToDoTask y )
        {
            return y.Difficulty - x.Difficulty;
        }
    }
}
