﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQPlay
{
    public class Stock
    {
        public string TickerSymbol { get; set; }
        public string StocName { get; set; }
        public decimal SharePrice { get; set; }
        public int UnitsOwned { get; set; }

        public Stock(string tickerSymbol, string stocName, decimal sharePrice, int unitsOwned)
        {
            TickerSymbol = tickerSymbol;
            StocName = stocName;
            SharePrice = sharePrice;
            UnitsOwned = unitsOwned;
        }
    }
}
