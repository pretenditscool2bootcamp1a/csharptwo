﻿using InterfacesExample;

namespace ClassPlay
{
    public class Program
    {
        static void Main(string[] args)
        {
            Animal[] animals =
            {
                new Giraffe { Name = "Gerry", Age = 4 },
                new Giraffe { Name = "Terry" , Age = 6},
                new Elephant { Name = "Dumbo", Age = 7},
                new Elephant { Name = "Dumbo's Mom", Age = 45},
                new Lion { Name = "Kimba" , Age = 13 , habitat = "savannah"},
                new Lion { Name = "Simba" , Age = 33 , habitat = "plains"}
            };
            foreach (Animal animal in animals)
            {
                Console.WriteLine("This animal's name is : {0} . It is : {1} years old. It makes the following sound: {2}", animal.Name, animal.Age, animal.MakeSound());
                if (animal is IHerbivore)
                {
                    Console.WriteLine(((IHerbivore)animal).Graze());
                }
                else if (animal is ICarnivore)
                {
                    Console.WriteLine(((ICarnivore)animal).Hunt());
                }
            }
        }
    }
}
