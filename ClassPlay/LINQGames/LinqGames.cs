﻿using LINQGames;

namespace PeopleManager
{


    public class LinqGames
    {



        static void Main(string[] args)
        {

            List<Supplier> suppliers = new List<Supplier> {
                new Supplier(101, "ACME", "acme.com"),
                new Supplier(201, "Spring Valley", "spring-valley.com"),
                new Supplier(301, "Spacely Sprocket", "spacelysprockets.com"),
                new Supplier(401, "Cosmo's Cogs", "cosmos-cogs.com"),
            };

            List<Product> products = new List<Product> {
                new Product(1, "Dark Chocolate Bar", 4.99M, 10, 101),
                new Product(2, "8 oz Guacamole", 5.99M, 27, 201),
                new Product(3, "Milk Chocolate Bar", 3.99M, 16, 101),
                new Product(4, "8 pkg Chicken Tacos", 15.99M, 7, 201),
                new Product(6, "French Toast", 7.99M, 15, 301),
                new Product(7, "Pancakes", 4.99M, 20, 301),
                new Product(8, "5inch Gasket", 1.15M, 100, 401),
                new Product(9, "10ft rubber tubing", 12.55M, 22, 401),
            };

            // exercise 1 names only of all suppliers 
            var supplierNamesQuery = from p in suppliers orderby p.Name ascending select p.Name;
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("Supplier names: ");
            foreach (string name in supplierNamesQuery)
            {
                Console.WriteLine(name);
            }

            // exercise 2 products < $5.00 
            var productsUnderFiveQuery = from p in products where p.Price < 5.00M orderby p.ProductName ascending select p.ProductName;
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("Products under $5.00 : ");
            foreach (string productName in productsUnderFiveQuery)
            {
                Console.WriteLine(productName);
            }
            // exercise 3 what products do we have 10 or more of in descending order by quantity
            var productsMoreThan10DescByQuantQuery = from p in products where p.QuantityOnHand >= 10 orderby p.QuantityOnHand descending select new
            {
                p.ProductName,
                p.QuantityOnHand
            } ;
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("Products with 10 or more on hand descending by quantity : ");
            foreach (var product in productsMoreThan10DescByQuantQuery)
            {
                Console.WriteLine("Product Name: {0}, Quantity On Hand: {1}", product.ProductName, product.QuantityOnHand);
            }

            // exercise 4 which is pg 111 ex 1 list all information about supplier 201
            var supplier201Query = from p in suppliers where p.SupplierId == 201 orderby p.Name ascending 
                                   select new { p.Name, p.SupplierId, p.URL };

            var supplier201ProductInformation = from p in products
                                                     where p.SupplierId == 201
                                                     orderby p.ProductName ascending
                                                     select new
                                                     {
                                                         p.ProductName,
                                                         p.QuantityOnHand,
                                                         p.Price
                                                     };
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine("list all information about supplier 201: ");
            foreach (var supplier in supplier201Query)
            {
                Console.WriteLine("Supplier Name: {0} SupplierId: {1} URL {2}", supplier.Name, supplier.SupplierId, supplier.URL);
            }
            
            foreach (var product in supplier201ProductInformation)
            {
                Console.WriteLine("Product Name: {0}, Quantity On Hand: {1}, Price {2:C} ", product.ProductName, product.QuantityOnHand, product.Price);
            }
            // exercise 5 which is pg 111 ex 2 what are the first product that costs $5 or more
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine(" what are the first product that costs $5 or more : ");
            var productGE5DollarsQuery = (from p in products where p.Price >= 5.00M select p.ProductName).First() ;
            
            Console.WriteLine("Product Name: {0}", productGE5DollarsQuery);

            // exercise 6 which is pg 111 ex 3 how many products cost $5 or more
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine(" how many products cost $5 or more : ");
            var productCountGE5DollarsQuery = (from p in products where p.Price >= 5.00M select p.ProductName).Count();

            Console.WriteLine("Product Count: {0}", productCountGE5DollarsQuery);

            // exercise 7 which is pg 111 ex 4 create a function and use it to display the list of products with 10 or less on hand
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine(" what items with less than 10 on hand need to be reordered : ");
            
            List < Product > reorderProducts = (from p in products
                                                    where p.QuantityOnHand <= 10
                                                    orderby p.ProductName ascending
                                                    select p).ToList();
            foreach (Product product in reorderProducts)
            {
                Console.WriteLine("ProductId : {0} Product Name: {1} Qunatity Onhand {2} Supplier ID {3}", product.ProductId, product.ProductName, product.QuantityOnHand, product.SupplierId);
            }

            // exercise 8 which is pg 111 ex 5 display the product name price and supplier name of all products
            Console.WriteLine("\n-------------------- ");
            Console.WriteLine(" display the product name price and supplier name of all products : ");

            var productSupplierJoinQuery = (from p in products 
                                            join s in suppliers on p.SupplierId equals s.SupplierId
                                            select new {p.ProductName, p.Price, s.Name}
                                            );
            foreach (var product in productSupplierJoinQuery)
            {
                Console.WriteLine("Product Name {0} Price {1} Supplier Name {2}", product.ProductName, product.Price, product.Name);
            }


            
        }
    }

    
}
