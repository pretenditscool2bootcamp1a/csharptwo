﻿int year = 2000;
Console.WriteLine("Enter a year ");
string inputYear = Console.ReadLine();
year = Convert.ToInt16(inputYear);
Console.WriteLine("Year = " + year);
if (DateTime.IsLeapYear(year))
{
    Console.WriteLine("Leap Year!");
}
else
{
    Console.WriteLine("Not a Leap Year!");
}
