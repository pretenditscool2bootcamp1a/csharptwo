﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesExample
{
    public sealed class Giraffe : Animal, IHerbivore
    {
        public override string Name { get ; set ; }
        public override int Age { get; set; }

        /*public Giraffe(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }*/
        public string Graze()
        {
            return "The giraffe stretches its long neck to reach leaves on tall trees.";
        }

        public override string MakeSound()
        {
            return "Hum";
        }
    }
}
